import axios from "axios";
import { Articles } from "@/entities/Articles";



export async function fetchAllArticles() {
    const response = await axios.get<Articles[]>('http://localhost:8000/api/article');
    return response.data;
}

export async function fetchOneArticle(id:number) {
    const response = await axios.get<Articles>('http://localhost:8000/api/article/'+id);
    return response.data;
}

export async function postArticle(Article:Articles) {
    const response = await axios.post<Articles>('http://localhost:8000/api/article', Article);
    return response.data;
}

export async function updateArticle(Article:Articles) {
    const response = await axios.put<Articles>('http://localhost:8000/api/article/'+Article.id, Article);
    return response.data;
}

export async function deleteArticle(id:any) {
    await axios.delete('http://localhost:8000/api/article/'+id);
}

