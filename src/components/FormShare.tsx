
import { Articles } from "@/entities/Articles"
import { useRouter } from "next/router"
import { FormEvent, useState } from "react"
import ValidationModal from "./ValidationModal";


interface Props {
    onSubmit:(article:Articles) => void;
    edited?:Articles;
}

export default function FormShare ({onSubmit,edited}:Props){
    const [errors, setErrors] = useState('');
    const [showForm, setShowForm] = useState(false);
    const router = useRouter()
    const [article, setArticle] = useState<Articles> (edited? edited:
        {
            name: '',
            title: '',
            description:'',
            picture:'',
            
        }
    )

    function handleChange(event: any) {
        setArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        
        try {
            onSubmit(article);

        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
      
      // Voir comment faire fonctionner la modale qui toggle quand on click sur effacer  
      //     function toggle(event:FormEvent) {
      //       event.preventDefault();
      //     setShowForm(!showForm);
      // }
  

}
return (
<>
    
    <form onSubmit={handleSubmit} className="m-4 p-3 text-center" id="form">
        {errors &&
        <p>{errors}</p>}
        <div className="row">
       <div className="border m-1"></div>
    <h3 className="text-danger mt-2">Publier votre article ici!</h3>
    </div>
  <div className="mb-3">
    <label htmlFor="name" className="form-label text-white">Nom de la série</label>
    <input type="text" name='name' value={article.name} onChange={handleChange} className="form-control" id="name" required/> 
  </div>
  <div className="mb-3">
    <label htmlFor="title" className="form-label text-white">Titre de l'article</label>
    <input type="text" name='title' value= {article.title} onChange={handleChange} className="form-control" id="title" required/>
  </div>
  <div className="mb-3">
    <label htmlFor="picture" className="form-label text-white">Photo</label>
    <input type="text" name='picture' value= {article.picture} onChange={handleChange} className="form-control" id="description" required/>
  </div>
  <div className="mb-3">
    <label htmlFor="description" className="form-label text-white">Description</label>
    <input type="text" name= 'description' value= {article.description} onChange={handleChange} className="form-control" id="description" required/>
  </div>
  {/* <ValidationModal onSubmit={toggle}/> */}
  <button type="submit" className="btn btn-danger m-2 col-3 btn-lg">Publier</button>

  </form>
</>
)


}


