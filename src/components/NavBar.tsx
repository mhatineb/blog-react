

const NavBar = ()=>{

    return (<>
    <nav className="navbar navbar-expand-lg bg-black shadow-1-strong">
  <div className="container-fluid ">
  <a className="navbar-brand" href="http://localhost:3000">
      <img src="/img/Logo.png" alt="Bootstrap" width="100" height="45"/>
    </a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
      
      <form className="d-flex " role="search">
        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
        <button className="btn btn-danger" type="submit">Rechercher</button>
      </form>
    </div>
  </div>
</nav>
    
    </>)
}

export default NavBar;