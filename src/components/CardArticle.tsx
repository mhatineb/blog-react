
import { fetchAllArticles } from '@/article-service';
import { Articles } from '@/entities/Articles';
import Link from 'next/link';
import { useEffect, useState } from 'react';



export default function CardArticle() {

    const [article, setArticle] = useState<Articles[]>([]);

    useEffect(() => {
        fetchAllArticles().then(data => {
            setArticle(data);
        })
    }, [])


    return (
        <>

            <div className="row bg-black g-2 m-2">

                {article.map(item =>

                    <div key={item.id} className="card col-sm-9 col-md-6 col-lg-3 mx-auto m-1">
                        <Link className="lien" href={"/article/" + item.id}>
                            <img src={item.picture} className="card-img-top" alt="photo de l'affiche de la série" />

                            <div className="card-body d-flex flex-column">
                                <h4 className="card-title">{item.name}</h4>
                                <h5 className="card-title">{item.title}</h5>
                            </div>

                            <div className="d-flex justify-content-sm-center mb-5">
                                <button className="btn btn-danger cardBtn mx-auto">Lire la suite</button>
                            </div>
                        </Link>
                    </div>
                )}
            </div>
        </>
    )
}