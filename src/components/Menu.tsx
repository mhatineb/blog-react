
import Link from "next/link";


const Menu = () => {
  return (<>

    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
      <div className="container-fluid bg-black d-flex justify-content-evenly">
        <li className=" nav-item text-white fst-italic">
          <Link className="nav-link active" aria-current="page" href="http://localhost:3000">
            <h5 className="menu">Accueil</h5></Link>
        </li>
        <li className="nav-item text-white fst-italic">
          <Link className="nav-link" href="#form">
            <h5 className="menu">Publier votre article</h5></Link>
        </li>
      </div>
    </ul>


  </>)
}

export default Menu;