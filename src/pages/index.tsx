
import { postArticle } from "@/article-service";
import CardArticle from "@/components/CardArticle";
import FormShare from "@/components/FormShare";
import Pagination from "@/components/Pagination";
import { Articles } from "@/entities/Articles";
import router from "next/router";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";



export default function Index() {

  async function add(article: Articles) {
    const added = await postArticle(article);
    router.push('/article/' + added.id);
  }

  return (<>
    <main className="bg-black shadow-1-strong">
      <div className="container g-1">


        <Pagination />

        <CardArticle />

        <FormShare onSubmit={add} />

      </div>

    </main>
  </>)

}
