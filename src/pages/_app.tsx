import type { AppProps } from 'next/app'
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../styles/globals.css";
import { useEffect } from "react";
import NavBar from '@/components/NavBar';
import Menu from '@/components/Menu';
import Footer from '@/components/Footer';





export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
    <>
      <NavBar />
      <h1 className="bg-black text-white text-center mt-2 p-5">Le meilleur de toutes les séries</h1>
      <Menu />
      <Component {...pageProps} />
      <Footer />

    </>
  );
}
