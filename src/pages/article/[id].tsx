import FormDog from "@/components/FormShare";
import { deleteArticle, fetchAllArticles, fetchOneArticle, updateArticle } from "@/article-service";
import { Articles } from "@/entities/Articles";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import FormShare from "@/components/FormShare";
import ValidationModal from "@/components/ValidationModal";



export default function ArticlePage() {
    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<Articles>();
    const [showEdit, setShowEdit] = useState(false);

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {

                if (error.response.status == 404) {
                    router.push('/404');
                }
            });


    }, [id]);

    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }

    async function update(article: Articles) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }

    function toggle() {
        setShowEdit(!showEdit);
    }

    if (!article) {
        return <p>Loading...</p>
    }


    return (
        <>
            <div className="row justify-content-center border-top-1">
                <h1 className="text-center mt-1">{article.name}</h1>
                <h2 className="text-center">{article.title}</h2>
                <img className="picture m-1" src={article.picture} />

                <div className="row col-10 mt-1">
                    <p className="">{article.description}</p>
                </div>

                <div className="row col-4">

                    <button className="btn btn-warning m-2 btn-lg" onClick={remove}>Effacer</button>

                    <button className="btn btn-warning m-2 btn-lg" onClick={toggle}>Modifier</button>

                </div>

                {showEdit &&
                    <>
                        <div className="row">
                            <div className="m-1">
                                <FormShare edited={article} onSubmit={update} />
                                {/* <ValidationModal onSubmit={toggle}/> */}
                            </div>
                        </div>
                    </>
                }
            </div>
        </>
    );
}

