

export interface Articles {

    id?: number;
    name?: string;
    title?: string;
    picture?: string;
    description?: string;
} 