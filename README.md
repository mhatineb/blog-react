
**Maquette Figma**
https://www.figma.com/file/35rLp0Kkt4nB4oP5zBJpxh/Projet-Blog?node-id=0%3A1&t=nfWU8DW7mlGSBJgg-0

**Diagramme de class StartUML dans le Trello**
https://trello.com/b/g1iIY0tr/blog

**React/Next.js**

Pour mon blog de séries, j'ai utilisé react/next.js

- Axios comme librairie afin de gérer mon fichier article-service et correspondre avec mon dossier blog symfony: - fetch:get
                          - post:post
                          - update:put
                          - delete:delete
 
- Gestion du CSS avec Bootstrap et un peu de CSS classique,

- Installation de react Bootstrap pour le component ValidationModal 

Fonctionnalités:

- Consulter les différents articles sur la page d'acceuil
- Publier un nouvel article via le formulaire 
- Consulter la description de l'article en page [id] et pouvoir faire des modifications via le bouton "modifier"
- Effacer un article via le bouton "effacer"


A finir:

- Modal pagination pour changer de page 
- La barre de recherche qui ne fonctionne pas pour l'instant
- Permette de confirmer "effacer" quand on appuie dessus avec le component ValidationModal